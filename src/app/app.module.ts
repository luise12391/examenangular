import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReduxModule } from './redux/redux.module';
import { UserService } from './services/user.service';
import { UserComponent } from './pages/user/user.component';
import { AlbumComponent } from './pages/album/album.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostComponent } from './pages/post/post.component';
import { CommentComponent } from './pages/comment/comment.component';
import { IndexComponent } from './pages/share/index/index.component';
import { ChangeTitlePipe } from './pipes/change-title.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    AlbumComponent,
    PostComponent,
    CommentComponent,
    IndexComponent,
    ChangeTitlePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ReduxModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
