import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Album } from '../models/album';
import { Post } from '../models/post';
import { Comment } from '../models/comment';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) { }
  
    getUsers():Observable<User[]> {
        return this.http
          .get<User[]>(
            environment.backendUrl + 'users',
            httpOptions
          );
    }

    getAlbums(id:number):Observable<Album[]> {
        return this.http
          .get<Album[]>(
            `${environment.backendUrl}users/${id}/albums`,
            httpOptions
          );
    }

    getPosts(id:number):Observable<Post[]> {
        return this.http
          .get<Post[]>(
            `${environment.backendUrl}users/${id}/posts`,
            httpOptions
          );
    }

    getComments(id:number):Observable<Comment[]> {
        return this.http
          .get<Comment[]>(
            `${environment.backendUrl}posts/${id}/comments`,
            httpOptions
          );
    }
}
