import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from 'src/app/models/user';
import { AppState } from 'src/app/redux/app.reducers';
import { UserActions } from 'src/app/redux/user/actions';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent {
    title:string;
    users:User[];
    constructor(private store: Store<AppState>){
        this.title = "Lista de Usuarios";

        this.store.dispatch(new UserActions.CargarUsuariosAction());
        
        this.store.select(state => state.usuarios.usuarios)
            .subscribe(usuarios =>{
            this.users = usuarios}               
        )
    }
}
