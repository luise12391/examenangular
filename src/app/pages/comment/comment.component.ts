import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/redux/app.reducers';
import { Comment } from 'src/app/models/comment';
import { ActivatedRoute } from '@angular/router';
import { UserActions } from 'src/app/redux/user/actions';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html'
})
export class CommentComponent {
  comments:Comment[];
  id:number;
  title: string;
  
  constructor(private fb: FormBuilder, private store: Store<AppState>, private route: ActivatedRoute){
    this.title = "Lista de comentarios del posts con id";
    this.id = this.route.snapshot.params.id;
    this.store.dispatch(new UserActions.CargarCommentsAction(this.id))
    this.store.select(state => state.usuarios.comments)
        .subscribe(comments =>{
        this.comments = comments}               
    )
  }
}