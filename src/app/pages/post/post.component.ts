import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Post } from 'src/app/models/post';
import { AppState } from 'src/app/redux/app.reducers';
import { UserActions } from 'src/app/redux/user/actions';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls:['./post.component.css']
})
export class PostComponent {
  posts:Post[];
  postForm: FormGroup;
  title: string;
  id:number;
  
  constructor(private fb: FormBuilder, private store: Store<AppState>){
    this.title = "Lista de posts";
    this.postForm = this.fb.group({
      userId: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*$')
      ]))
    });

    this.store.select(state => state.usuarios.posts)
        .subscribe(posts =>{
        this.posts = posts
      }               
    )
  }
  public checkError = (controlName: string, errorName: string) => {
    return this.postForm.controls[controlName].hasError(errorName);
  }

  getPosts(){
    var userId= this.postForm.controls['userId'].value;
    if(!this.checkError('userId', 'pattern') && !isNaN(userId)){
      this.store.dispatch(new UserActions.CargarPostsAction(userId));   
      this.id = userId;   
    }  
  }
}