import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Album } from 'src/app/models/album';
import { AppState } from 'src/app/redux/app.reducers';
import { UserActions } from 'src/app/redux/user/actions';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls:['./album.component.css']
})
export class AlbumComponent {
  albums:Album[];
  usuarioForm: FormGroup;
  title:string;
  id:number;
  constructor(private fb: FormBuilder, private store: Store<AppState>){
    this.title = "Lista de albums";
    this.usuarioForm = this.fb.group({
      userId: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*$')
      ]))
    });

    this.store.select(state => state.usuarios.albums)
        .subscribe(albums =>{
        this.albums = albums
      }               
    )
  }

  getAlbums(){
    var userId= this.usuarioForm.controls['userId'].value;
    if(!this.checkError('userId', 'pattern') && !isNaN(userId)){
      this.store.dispatch(new UserActions.CargarAlbumsAction(userId));  
      this.id = userId; 
    }  
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.usuarioForm.controls[controlName].hasError(errorName);
  }
}