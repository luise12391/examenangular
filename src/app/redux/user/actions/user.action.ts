import { Action } from '@ngrx/store';
import { Album } from 'src/app/models/album';
import { Post } from 'src/app/models/post';
import { User } from '../../../models/user';
import { Comment } from '../../../models/comment';

export enum UserActionTypes {
  INICIALIZA_USUARIOS = '[Usuarios] Inicializar lista usuarios',
  CARGAR_USUARIOS = '[Usuarios] Cargar usuarios',
  INICIALIZA_ALBUMS = '[Usuarios] Inicializar lista albums',
  CARGAR_ALBUMS = '[Usuarios] Cargar albums',
  INICIALIZA_POSTS = '[Usuarios] Inicializar lista posts',
  CARGAR_POSTS = '[Usuarios] Cargar posts',
  INICIALIZA_COMMENT = '[Usuarios] Inicializar lista comments',
  CARGAR_COMMENT = '[Usuarios] Cargar comments',
}

export class InicializaUsuariosAction implements Action {
  type = UserActionTypes.INICIALIZA_USUARIOS;
  constructor(public user: User[]) {}
}

export class CargarUsuariosAction implements Action {
  type = UserActionTypes.CARGAR_USUARIOS;
  constructor() {}
}

export class InicializaAlbumsAction implements Action {
  type = UserActionTypes.INICIALIZA_ALBUMS;
  constructor(public albums: Album[]) {}
}

export class CargarAlbumsAction implements Action {
  type = UserActionTypes.CARGAR_ALBUMS;
  constructor(public id:number) {}
}

export class InicializaPostsAction implements Action {
  type = UserActionTypes.INICIALIZA_POSTS;
  constructor(public posts: Post[]) {}
}

export class CargarPostsAction implements Action {
  type = UserActionTypes.CARGAR_POSTS;
  constructor(public id:number) {}
}

export class InicializaCommentsAction implements Action {
  type = UserActionTypes.INICIALIZA_COMMENT;
  constructor(public comments: Comment[]) {}
}

export class CargarCommentsAction implements Action {
  type = UserActionTypes.CARGAR_COMMENT;
  constructor(public id:number) {}
}
export type UsuariostosActionsAllTypes = InicializaUsuariosAction 
                                        | CargarUsuariosAction
                                        | InicializaAlbumsAction 
                                        | CargarAlbumsAction
                                        | InicializaPostsAction
                                        | CargarPostsAction
                                        | InicializaCommentsAction
                                        | CargarCommentsAction;