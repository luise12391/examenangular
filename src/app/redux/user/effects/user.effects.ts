import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { EMPTY } from "rxjs";
import { catchError, map, mergeMap } from "rxjs/operators";
import { Album } from "src/app/models/album";
import { Post } from "src/app/models/post";
import { User } from "src/app/models/user";
import { Comment } from "src/app/models/comment";
import { UserService } from "src/app/services/user.service";
import { CargarAlbumsAction, CargarCommentsAction, CargarPostsAction, InicializaAlbumsAction, InicializaCommentsAction, InicializaPostsAction, InicializaUsuariosAction, UserActionTypes } from "../actions/user.action";

//EFFECTS
@Injectable({
    providedIn: 'root'
})
export class UserEffects {

    obtenerUsuarios$  = createEffect(() =>  this.actions$.pipe(
        ofType(UserActionTypes.CARGAR_USUARIOS),
        mergeMap(() => this.userService.getUsers()
            .pipe(
                map((usuarios:User[]) => new InicializaUsuariosAction(usuarios)),
                catchError(() => EMPTY)
            ))
        )
    );

    obtenerAlbums$  = createEffect(() =>  this.actions$.pipe(
        ofType(UserActionTypes.CARGAR_ALBUMS),
        mergeMap((action:CargarAlbumsAction) => this.userService.getAlbums(action.id)
            .pipe(
                map((albums:Album[]) => new InicializaAlbumsAction(albums)),
                catchError(() => EMPTY)
            ))
        )
    );

    obtenerPosts$  = createEffect(() =>  this.actions$.pipe(
        ofType(UserActionTypes.CARGAR_POSTS),
        mergeMap((action:CargarPostsAction) => this.userService.getPosts(action.id)
            .pipe(
                map((posts:Post[]) => new InicializaPostsAction(posts)),
                catchError(() => EMPTY)
            ))
        )
    );

    obtenerComments$  = createEffect(() =>  this.actions$.pipe(
        ofType(UserActionTypes.CARGAR_COMMENT),
        mergeMap((action:CargarCommentsAction) => this.userService.getComments(action.id)
            .pipe(
                map((comments:Comment[]) => new InicializaCommentsAction(comments)),
                catchError(() => EMPTY)
            ))
        )
    );
    
    constructor(private actions$: Actions, private userService: UserService ) {}
}
