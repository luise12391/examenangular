import { UserActions } from '../actions';
import { User } from '../../../models/user';
import { UserActionTypes, InicializaUsuariosAction, InicializaAlbumsAction, InicializaPostsAction, InicializaCommentsAction } from '../actions/user.action';
import { Album } from 'src/app/models/album';
import { Post } from 'src/app/models/post';
import { Comment } from 'src/app/models/comment';

export interface UserState{
  usuarios: User[];
  albums: Album[];
  posts: Post[];
  comments: Comment[]
}

export function intializeUserState() {
  return {
    usuarios: [],
    albums: [],
    posts:[],
    comments:[]
  };
}
  //REDUCERS
export function UserReducer(
	state:UserState,
	action:UserActions.UsuariostosActionsAllTypes
) : UserState {
	switch (action.type) {
        case UserActionTypes.INICIALIZA_USUARIOS:{
            var users = [...(action as InicializaUsuariosAction).user];
            if (users == undefined)
              users = [];
            return {
                ...state,
                usuarios :users,
            };
        }
        case UserActionTypes.INICIALIZA_ALBUMS:{
          var albums = [...(action as InicializaAlbumsAction).albums];
          if (albums == undefined)
            albums = [];
          return {
              ...state,
              albums :albums,
          };
        }
        case UserActionTypes.INICIALIZA_POSTS:{
          var posts = [...(action as InicializaPostsAction).posts];
          if (posts == undefined)
          posts = [];
          return {
              ...state,
              posts :posts,
          };
        }
        case UserActionTypes.INICIALIZA_COMMENT:{
          var comments = [...(action as InicializaCommentsAction).comments];
          if (comments == undefined)
          comments = [];
          return {
              ...state,
              comments :comments,
          };
        }
	}
	return state;
}
