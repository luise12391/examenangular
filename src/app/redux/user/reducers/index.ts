import * as appReducer from '../../app.reducers';
import { createSelector, ActionReducerMap, createFeatureSelector } from '@ngrx/store';


// se tiene que exportar así para poder hacer ng build --prod.
import * as fromUsuarios from './user.reducer';


export interface UserState {
    usuarios: fromUsuarios.UserState;
} fromUsuarios

export interface State extends appReducer.AppState {
    USUARIOS: UserState;
}

export const usuariosReducers: ActionReducerMap<UserState> = {
    usuarios: fromUsuarios.UserReducer
};

export const selectUsersState = createFeatureSelector<State, UserState>('USUARIOS');


export const selectUsers = createSelector(
    selectUsersState,
    (state: UserState) => state.usuarios
);
