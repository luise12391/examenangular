import {
    ActionReducerMap,
    createSelector,
    createFeatureSelector,
    ActionReducer,
    MetaReducer,
  } from '@ngrx/store';
  import { environment } from '../../environments/environment';
  import { User } from '../models/user';
  import { UserState, UserReducer } from './user/reducers/user.reducer';
  
  
  export interface AppState {
    usuarios: UserState;
  };
  
  export const appReducers: ActionReducerMap<AppState> = {
    usuarios: UserReducer
  };
  