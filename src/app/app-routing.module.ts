import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumComponent } from './pages/album/album.component';
import { CommentComponent } from './pages/comment/comment.component';
import { PostComponent } from './pages/post/post.component';
import { UserComponent } from './pages/user/user.component';

const routes: Routes = [
  { path: '', component: UserComponent },
  { path: 'users', component: UserComponent },
  { path: 'albums', component: AlbumComponent },
  { path: 'posts', component: PostComponent },
  { path: 'comments/:id', component: CommentComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
