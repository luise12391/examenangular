import { Component } from '@angular/core';
import {ChangeTitlePipe} from './pipes/change-title.pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'Test de angular';
}
